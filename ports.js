'use strict'

// Get permission to show desktop notifications
// Per https://stackoverflow.com/questions/2271156/chrome-desktop-notification-example
document.addEventListener('DOMContentLoaded', function() {
 if (!Notification) {
  return;
 }

 if (Notification.permission !== 'granted')
  Notification.requestPermission();
});


// get a reference to the div where we will show our UI
let container = document.getElementById('container')


// start the elm app in the container
// and keep a reference for communicating with the app
var app = Elm.Main.init({
    node: container,
    flags: {}
});

app.ports.playSound.subscribe(function (filename) {
    var audio = new Audio('assets/'.concat(filename));
    audio.play();
});

app.ports.desktopNotification.subscribe(function (text) {
 if (Notification.permission !== 'granted')
  Notification.requestPermission();
 else {
  var notification = new Notification('Timer', {
   body: text
  });
  notification.onclick = function() {
   window.open('http://stackoverflow.com/a/13328397/1269037');
  };
 }
});