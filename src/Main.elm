module Main exposing (main)

import Browser
import Element
import Helpers
import Html
import Ports
import Task
import Time
import TimeHelpers
    exposing
        ( toHumanTime
        , toPhase
        )
import Types
    exposing
        ( Model
        , Msg(..)
        , Phase(..)
        )


main : Program {} Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : {} -> ( Model, Cmd Msg )
init _ =
    -- Model is initially populated with default, certainly incorrect values
    -- Until we know the correct ones
    ( { curTime = Time.millisToPosix 0, zone = Time.utc }
    , Cmd.batch
        [ Task.perform NewTime Time.now
        , Task.perform Zone Time.here
        ]
    )


view : Model -> Html.Html Msg
view model =
    Element.layout [] <|
        Element.column
            []
            [ Element.text ("The time is " ++ toHumanTime model.curTime model.zone)
            , Element.text ("The phase is " ++ Debug.toString (toPhase model.curTime model.zone))
            , Element.text "You gotta click in the browser window for the chime to play"
            ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewTime newTime ->
            let
                curPhase =
                    toPhase newTime model.zone

                prevPhase =
                    toPhase model.curTime model.zone

                cmd =
                    if curPhase /= prevPhase then
                        let
                            soundFileName =
                                curPhase
                                    |> Helpers.soundForPhase
                                    |> Helpers.soundFileName
                        in
                        Cmd.batch
                            [ Ports.playSound soundFileName
                            , Ports.desktopNotification (Debug.toString curPhase)
                            ]

                    else
                        Cmd.none
            in
            ( { model
                | curTime = newTime
              }
            , cmd
            )

        Zone zone ->
            ( { model | zone = zone }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    -- Update twice a second
    Time.every (1 * 500) NewTime
