module Types exposing (Model, Msg(..), Phase(..), Sound(..))

import Dict
import Time


type alias Model =
    { curTime : Time.Posix
    , zone : Time.Zone
    }


type Msg
    = NewTime Time.Posix
    | Zone Time.Zone


type Phase
    = WorkTime
    | BreakTime


type Sound
    = TimerBeep
    | DJAirHorn
