module Helpers exposing (soundFileName, soundForPhase)

import Types exposing (..)


soundFileName : Sound -> String
soundFileName sound =
    case sound of
        TimerBeep ->
            "timer-beep.ogg"

        DJAirHorn ->
            "dj-air-horn.m4a"


soundForPhase : Phase -> Sound
soundForPhase phase =
    case phase of
        BreakTime ->
            TimerBeep

        WorkTime ->
            TimerBeep
