module TimeHelpers exposing (toHumanTime, toPhase)

import Time
    exposing
        ( Posix
        , Zone
        , toHour
        , toMinute
        , toSecond
        )
import Types exposing (Phase(..))


toHumanTime : Posix -> Zone -> String
toHumanTime time zone =
    let
        timeParts =
            [ toHour, toMinute, toSecond ]

        toHumanTimePart : (Zone -> Posix -> Int) -> String
        toHumanTimePart timePartFunc =
            timePartFunc zone time
                |> String.fromInt
                |> String.padLeft 2 '0'
    in
    timeParts
        |> List.map toHumanTimePart
        |> String.join ":"


toPhase : Time.Posix -> Time.Zone -> Phase
toPhase time zone =
    let
        minute =
            toMinute zone time
    in
    if minute < 5 then
        BreakTime

    else if minute < 30 then
        WorkTime

    else if minute < 35 then
        BreakTime

    else
        WorkTime
